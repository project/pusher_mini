<?php

declare(strict_types = 1);

namespace Drupal\Tests\pusher_mini\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\key\Entity\Key;
use Drupal\pusher_mini\HookPageBottom;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests Pusher Mini.
 */
final class PusherMiniTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pusher_mini',
    'key',
    'key_test',
    'user',
    'system',
  ];

  /**
   * Test auths appear in HTML response as JS.
   */
  public function testsPusherJs(): void {
    $this->setUpCurrentUser(permissions: [
      'pusher_mini use',
      'pusher_mini authenticate',
    ]);

    $key = Key::create([
      'id' => 'test_key',
      'label' => 'Test key',
      'description' => 'Test pusher key',
      'key_type' => 'pusher_mini',
      'key_type_settings' => [],
      'key_provider' => 'key_test_state',
      'key_provider_settings' => [
        'state_key' => 'test_multivalue',
      ],
      'key_input' => 'pusher_key_input',
      'key_test_multi_settings' => [
        'app_key' => '',
        'app_secret' => '',
      ],
    ]);

    // Ignore bad phpdoc: https://www.drupal.org/project/key/issues/3395817.
    // @phpstan-ignore-next-line
    $key->setKeyValue([
      'app_key' => 'TESTAPPKEY',
      'app_secret' => 'TESTAPPSECRET',
    ]);
    $key->save();

    $this->config('pusher_mini.settings')
      ->set('key_id', 'test_key')
      ->set('disable_auth', FALSE)
      ->set('auth_route', '/foo/pusher-mini/authenticate/path')
      ->set('app_cluster', 'xyz')
      ->save();

    /** @var \Drupal\pusher_mini\HookPageBottom $hook */
    $hook = \Drupal::service(HookPageBottom::class);
    $bottom = [];
    $hook->hookPageBottom($bottom);

    static::assertEquals(<<<JS
      window.PusherConfiguration = {"appKey":"TESTAPPKEY","options":{"cluster":"xyz","forceTLS":null,"enableStats":null,"enabledTransports":["ws","wss"],"userAuthentication":{"endpoint":"\/foo\/pusher-mini\/authenticate\/path"}}}
      JS, $bottom['pusher_mini']['auth']['#value']);
  }

}
