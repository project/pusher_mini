<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pusher_mini\AuthenticatedUserInterface;
use Pusher\PusherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * User authentication for Pusher.
 */
final class PusherAuth extends ControllerBase {

  /**
   * Constructor.
   */
  final private function __construct(
    private PusherInterface $pusher,
    private readonly AuthenticatedUserInterface $authenticatedUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      pusher: $container->get(PusherInterface::class),
      authenticatedUser: $container->get(AuthenticatedUserInterface::class),
    );
  }

  /**
   * Authenticates a request from JS client library with Pusher.
   */
  public function __invoke(Request $request): JsonResponse {
    $socketId = $request->request->get('socket_id');
    if (!is_string($socketId) || strlen($socketId) > 32) {
      throw new BadRequestHttpException();
    }

    // See https://github.com/pusher/pusher-http-php/pull/379.
    // @phpstan-ignore-next-line
    $result = $this->pusher->authenticateUser($socketId, [
      'id' => $this->authenticatedUser->getUserId($this->currentUser()),
      'user_info' => $this->authenticatedUser->getUserInfo($this->currentUser()),
      'watchlist' => [],
    ]);

    return new JsonResponse($result, json: TRUE);
  }

}
