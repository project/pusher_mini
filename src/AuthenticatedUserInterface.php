<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini;

use Drupal\Core\Session\AccountInterface;

/**
 * Determine user authentication information for Pusher.
 */
interface AuthenticatedUserInterface {

  /**
   * Get user ID string from user account.
   */
  public function getUserId(AccountInterface $account): string;

  /**
   * Get user info from user account.
   */
  public function getUserInfo(AccountInterface $account): array;

}
