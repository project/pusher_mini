<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini\Plugin\KeyInput;

use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyInputBase;

/**
 * Defines a multi-value key input class.
 *
 * @KeyInput(
 *   id = "pusher_key_input",
 *   label = @Translation("Pusher key input multivalue")
 * )
 */
final class PusherKeyInput extends KeyInputBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'app_key' => '',
      'app_secret' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return $form + [
      'app_key' => [
        '#type' => 'textfield',
        '#default_value' => $this->configuration['app_key'],
        '#title' => $this->t('App key'),
      ],
      'app_secret' => [
        '#type' => 'textfield',
        '#default_value' => $this->configuration['app_secret'],
        '#title' => $this->t('App secret'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processSubmittedKeyValue(FormStateInterface $form_state): array {
    return [
      // Trick \Drupal\key\Form\KeyFormBase::submitForm submitted!=obscured.
      'submitted' => 'foo',
      'processed_submitted' => $form_state->getValues(),
    ];
  }

}
