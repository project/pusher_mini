<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini\Plugin\KeyType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyTypeBase;
use Drupal\key\Plugin\KeyTypeMultivalueInterface;

/**
 * Defines a key type that is multi-value.
 *
 * @KeyType(
 *   id = \Drupal\pusher_mini\Plugin\KeyType\PusherKey::PLUGIN_ID,
 *   label = @Translation("Pusher"),
 *   description = @Translation("Store Pusher app keys."),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "pusher_key_input",
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "app_key" = {
 *         "label" = @Translation("App key"),
 *         "required" = true
 *       },
 *       "app_secret" = {
 *         "label" = @Translation("App secret"),
 *         "required" = true
 *       },
 *     }
 *   }
 * )
 */
final class PusherKey extends KeyTypeBase implements KeyTypeMultivalueInterface {

  public const PLUGIN_ID = 'pusher_mini';

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(
    array $form,
    FormStateInterface $form_state,
    $key_value
  ): void {
  }

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return \json_encode([
      'app_key' => '',
      'app_secret' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function serialize(array $array) {
    return \json_encode($array);
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($value) {
    return json_decode($value, TRUE);
  }

}
