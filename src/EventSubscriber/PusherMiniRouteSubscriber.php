<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Modifies routes by config.
 */
final class PusherMiniRouteSubscriber extends RouteSubscriberBase {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Removes auth route and modifies auth path by config.
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $config = $this->configFactory->get('pusher_mini.settings');
    $disableAuthRoute = $config->get('disable_auth');
    $authRoute = $config->get('auth_route');

    if ($disableAuthRoute === TRUE) {
      $collection->remove('pusher_mini.auth');
      return;
    }

    // String length must include at least a slash + another character.
    if (is_string($authRoute) && strlen($authRoute) >= 2) {
      $collection
        ->get('pusher_mini.auth')
        ->setPath($authRoute);
    }
  }

}
