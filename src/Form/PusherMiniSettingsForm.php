<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\pusher_mini\Plugin\KeyType\PusherKey;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Pusher Mini.
 */
final class PusherMiniSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * Constructs a new PusherMiniSettingsForm.
   */
  private function __construct(
    ConfigFactoryInterface $configFactory,
    private RequestContext $requestContext,
    protected RouteBuilderInterface $routeBuilder,
  ) {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      configFactory: $container->get('config.factory'),
      requestContext: $container->get('router.request_context'),
      routeBuilder: $container->get('router.builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pusher_mini_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pusher_mini.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('pusher_mini.settings');

    $form['key_id'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Create a key with the <em>Pusher</em> key type.'),
      '#default_value' => $config->get('key_id'),
      '#config_data_store' => 'pusher_mini.settings:key_id',
      '#key_filters' => [
        'type' => PusherKey::PLUGIN_ID,
      ],
    ];

    $form['disable_auth'] = [
      '#title' => $this->t('Enable <a href=":url" target="_blank">user authentication route</a>.', [
        ':url' => 'https://pusher.com/docs/channels/using_channels/user-authentication/',
      ]),
      '#type' => 'checkbox',
      '#default_value' => !((bool) $config->get('disable_auth')),
    ];

    $form['auth_route'] = [
      '#title' => $this->t('User authentication route.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('auth_route'),
      '#field_prefix' => $this->requestContext->getCompleteBaseUrl(),
      '#size' => 32,
    ];

    $form['app_id'] = [
      '#title' => $this->t('App ID.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('app_id'),
      '#size' => 16,
    ];

    $form['app_cluster'] = [
      '#title' => $this->t('The <a href=":url" target="_blank">app cluster</a>.', [
        ':url' => 'https://pusher.com/docs/channels/miscellaneous/clusters/#what-clusters-exist?',
      ]),
      '#type' => 'textfield',
      '#default_value' => $config->get('app_cluster'),
      '#size' => 4,
    ];

    $form['server']['#type'] = 'fieldset';
    $form['server']['#tree'] = TRUE;
    $form['server']['#title'] = $this->t('Server connection');
    $form['server']['description'] = [
      '#markup' => $this->t('Leave empty if using Pusher.com services.'),
    ];

    $form['server']['scheme'] = [
      '#title' => $this->t('Scheme'),
      '#type' => 'textfield',
      '#default_value' => $config->get('server.scheme'),
    ];
    $form['server']['host'] = [
      '#title' => $this->t('Host'),
      '#type' => 'textfield',
      '#default_value' => $config->get('server.host'),
    ];
    $form['server']['port'] = [
      '#title' => $this->t('Port'),
      '#type' => 'textfield',
      '#default_value' => $config->get('server.port'),
    ];
    $form['server']['timeout'] = [
      '#title' => $this->t('Timeout'),
      '#type' => 'textfield',
      '#default_value' => $config->get('server.timeout'),
    ];
    $form['server']['useTls'] = [
      '#title' => $this->t('Use TLS'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('server.useTls'),
    ];

    $form['client']['#type'] = 'fieldset';
    $form['client']['#tree'] = TRUE;
    $form['client']['#title'] = $this->t('Client connection');
    $form['client']['description'] = [
      '#markup' => $this->t('Leave empty if using Pusher.com services.'),
    ];

    $form['client']['wsHost'] = [
      '#title' => $this->t('wsHost'),
      '#type' => 'textfield',
      '#default_value' => $config->get('client.wsHost'),
    ];
    $form['client']['wsPort'] = [
      '#title' => $this->t('wsPort'),
      '#type' => 'textfield',
      '#default_value' => $config->get('client.wsPort'),
    ];
    $form['client']['wssPort'] = [
      '#title' => $this->t('wssPort'),
      '#type' => 'textfield',
      '#default_value' => $config->get('client.wssPort'),
    ];
    $form['client']['forceTLS'] = [
      '#title' => $this->t('forceTLS'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('client.forceTLS'),
    ];
    $form['client']['enableStats'] = [
      '#title' => $this->t('Enable stats'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('client.enableStats'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $path = $form_state->getValue('auth_route');
    if (FALSE === str_starts_with($path, '/')) {
      $form_state->setError($form['auth_route'], $this->t("Path must begin with a '/' character."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('pusher_mini.settings')
      ->set('key_id', $form_state->getValue('key_id'))
      ->set('disable_auth', !((bool) $form_state->getValue('disable_auth')))
      ->set('auth_route', $form_state->getValue('auth_route'))
      ->set('app_id', $form_state->getValue('app_id'))
      ->set('app_cluster', $form_state->getValue('app_cluster'))
      ->set('server.scheme', $form_state->getValue(['server', 'scheme']))
      ->set('server.host', $form_state->getValue(['server', 'host']))
      ->set('server.port', $form_state->getValue(['server', 'port']))
      ->set('server.timeout', $form_state->getValue(['server', 'timeout']))
      ->set('server.useTls', $form_state->getValue(['server', 'useTls']))
      ->set('client.wsHost', $form_state->getValue(['client', 'wsHost']))
      ->set('client.wsPort', $form_state->getValue(['client', 'wsPort']))
      ->set('client.wssPort', $form_state->getValue(['client', 'wssPort']))
      ->set('client.forceTLS', $form_state->getValue(['client', 'forceTLS']))
      ->set('client.enableStats', $form_state->getValue(['client', 'enableStats']))
      ->save();

    $this->routeBuilder->setRebuildNeeded();

    parent::submitForm($form, $form_state);
  }

}
