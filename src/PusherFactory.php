<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;
use Pusher\Pusher;

/**
 * Factory for Pusher instances.
 *
 * @see \Pusher\Pusher
 */
final class PusherFactory {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly ClientInterface $client,
    private readonly KeyRepositoryInterface $keyRepository,
  ) {
  }

  /**
   * Factory for Pusher objects.
   */
  public function createPusher(): Pusher {
    $appKey = '';
    $appSecret = '';

    $config = $this->configFactory->get('pusher_mini.settings');
    $key = $config->get('key_id');
    if (is_string($key) && strlen($key) > 0) {
      $key = $this->keyRepository->getKey($key);
      if ($key instanceof KeyInterface) {
        /** @var array{app_key: string, app_secret: string} $values */
        $values = $key->getKeyValues();
        ['app_key' => $appKey, 'app_secret' => $appSecret] = $values;
      }
    }

    $options = [
      'cluster' => $config->get('app_cluster'),
      'useTLS' => TRUE,
    ];

    $host = $config->get('server.host');
    if (strlen($host) > 0) {
      $options['useTLS'] = $config->get('server.useTls');
      $options['scheme'] = $config->get('server.scheme');
      $options['host'] = $config->get('server.host');
      $options['port'] = $config->get('server.port');
      $options['timeout'] = $config->get('server.timeout');
    }

    return new Pusher(
      $appKey,
      $appSecret,
      $config->get('app_id'),
      $options,
      $this->client,
    );
  }

}
