<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini;

use Drupal\Core\Session\AccountInterface;

/**
 * Determine user authentication information for Pusher.
 */
final class AuthenticatedUser implements AuthenticatedUserInterface {

  /**
   * {@inheritdoc}
   */
  public function getUserId(AccountInterface $account): string {
    return (string) $account->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo(AccountInterface $account): array {
    return [
      'name' => $account->getAccountName(),
    ];
  }

}
