<?php

declare(strict_types = 1);

namespace Drupal\pusher_mini;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Service for hook_page_bottom().
 *
 * This service is internal and its behaviour and existence are subject to
 * change at any time.
 *
 * @internal
 */
final class HookPageBottom {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly KeyRepositoryInterface $keyRepository,
    private readonly AccountInterface $currentUser,
  ) {
  }

  /**
   * Implements hook_page_bottom().
   */
  public function hookPageBottom(array &$pageBottom): void {
    // @todo service is session-bound. Requires reset. One invocation per
    // request expected so deprio.
    if (FALSE === $this->currentUser->hasPermission('pusher_mini use')) {
      return;
    }

    $config = $this->configFactory->get('pusher_mini.settings');

    $data = [
      'appKey' => 'app-key',
      'options' => [
        'cluster' => $config->get('app_cluster'),
        'forceTLS' => $config->get('client.forceTLS'),
        'enableStats' => $config->get('client.enableStats'),
        'enabledTransports' => ['ws', 'wss'],
      ],
    ];

    $clientWsHost = $config->get('client.wsHost');
    if (is_string($clientWsHost) && strlen($clientWsHost) > 0) {
      $data['options'] += [
        'wsHost' => $config->get('client.wsHost'),
        'wsPort' => $config->get('client.wsPort'),
        'wssPort' => $config->get('client.wssPort'),
      ];
    }

    $key = $config->get('key_id');
    if (is_string($key) && strlen($key) > 0) {
      $key = $this->keyRepository->getKey($key);
      if ($key instanceof KeyInterface) {
        /** @var array{app_key: string, app_secret: string} $values */
        $values = $key->getKeyValues();
        ['app_key' => $appKey] = $values;
        if (strlen($appKey) > 0) {
          $data['appKey'] = $appKey;
        }
      }
    }

    if (FALSE === $config->get('disable_auth')) {
      try {
        $url = Url::fromRoute('pusher_mini.auth');
        $data['options']['userAuthentication']['endpoint'] = $url->toString();
      }
      // The route exists conditionally depending on configuration.
      // See PusherMiniRouteSubscriber.
      catch (RouteNotFoundException) {
      }
    }

    $json = Json::encode($data);
    $element = [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#value' => <<<JS
        window.PusherConfiguration = {$json}
        JS,
    ];

    $pageBottom['pusher_mini'] = [
      'auth' => $element,
    ];
    (new CacheableMetadata())
      // Conditionally visible via 'pusher_mini use'.
      ->addCacheContexts(['user.permissions'])
      ->addCacheTags(['config:pusher_mini.settings'])
      ->applyTo($pageBottom['pusher_mini']);
  }

}
