**Pusher Mini**

https://www.drupal.org/project/pusher_mini

This module is intended for developer purposes only. It does not provide any end
user features on its own. You will only need this module if another module
requests it.

# Requirements

This project uses the Pusher packagist library. You should install the module
with Composer so your dependencies are managed automatically. Support is not
provided for non-Composer installations.

# Installation

Install the module as you would with any other Drupal project.

Enabling the module will force the Key project to be installed if it is not
already.

# Configuration

1. Add your API at /admin/config/system/keys/add.
2. Under *Key type*, select _Pusher_ option.
3. Enter your _App key_ and _App secrets_ into the fields.
4. Save the form.
5. Configure Pusher to use your key at /admin/config/services/pusher-mini.
6. Select the previously created Pusher key under *API key*.
7. Save the form.

# Usage

## Pusher Client

The Pusher Client is intentionally not included with this project. You may
bundle it with your site in any way you like. This project passes along Pusher
Client configuration to the browser via `window.PusherConfiguration` when the
user in context has `pusher_mini use` permission.

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
